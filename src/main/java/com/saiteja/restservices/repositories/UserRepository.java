package com.saiteja.restservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saiteja.restservices.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	 User findUserByUsername(String username);

}
